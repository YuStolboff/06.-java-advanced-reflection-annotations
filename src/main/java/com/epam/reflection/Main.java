package com.epam.reflection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        try {
            Class aClass = Class.forName("com.epam.reflection.Entity");
            Entity entity = (Entity) aClass.newInstance();
            log.info(String.valueOf(entity));

            Method[] methods = aClass.getDeclaredMethods();
            for (Method method : methods) {
                log.info("Method: {}", String.valueOf(method.getName()));
                log.info("Method parameter: {}", Arrays.toString(method.getParameters()));
            }
            for (Method method : methods) {
                Annotation[] annotations = method.getDeclaredAnnotations();
                for (Annotation annotation : annotations) {
                    log.info("Annotation: {}, In method: {}", annotation, method.getName());
                }
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            log.info("Exception: ", e);
        }
    }
}
