#Practice

##Reflection/Annotations processing

Create annotation "Secured" for methods: two parameters, one mandatory of Integer type and optional String with a default value “strict”.

Enumerate all methods when one of parameters is of Integer type. Create a class ‘Entity’ with 3+ methods (public, private).

Apply annotation for two methods of the Entity(first case - only mandatory annotation parameters, second - set optional too).

Create instance of this class using hardcoded string class name.

Process instance of this class in runtime, print out all methods of it, for each method show annotation presense and its parameters.