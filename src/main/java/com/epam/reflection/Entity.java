package com.epam.reflection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Entity {

    private static final Logger log = LoggerFactory.getLogger(Entity.class);

    @Secured(4)
    private void privateMethodFirst(Integer integer) {
        log.info("privateMethodFirst");

    }

    @Secured(value = 5, name = "some")
    public void publicMethodFirst(Integer integer) {
        log.info("publicMethodFirst");

    }

    private void privateMethodSecond(String string) {
        log.info("privateMethodSecond");

    }

    public void publicMethodSecond(int number, String string) {
        log.info("privateMethodSecond");
    }
}

